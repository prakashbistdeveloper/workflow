package com.beeast.workflow.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.springframework.stereotype.Component;

@Component
public class DateUtil {

	private DateFormat dateFormatWithOutTime, dateFormatWithTime;

	public DateUtil() {
		dateFormatWithOutTime = new SimpleDateFormat("yyyy-MM-dd");
		dateFormatWithTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	}

	public String formatDateWithOutTime(Date sourceDate) {
		return dateFormatWithOutTime.format(sourceDate);
	}

	public Date formatDateWithOutTime(String sourceDate) {
		try {
			return dateFormatWithOutTime.parse(sourceDate);
		} catch (ParseException e) {
		}
		return null;
	}

	public String formatDateWithTime(Date sourceDate) {
		return dateFormatWithTime.format(sourceDate);
	}

	public Date formatDateWithTime(String sourceDate) {
		try {
			return dateFormatWithTime.parse(sourceDate);
		} catch (ParseException e) {
		}
		return null;
	}
}
