package com.beeast.workflow.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TempDataUtil implements Serializable {

	@Autowired
	private DateUtil dateUtil;

	public JSONObject prepareTempData(String date) {
		JSONObject data = new JSONObject();
		try {
			Date lastUpdated = dateUtil.formatDateWithTime(date);
			System.out.println(date);
			System.out.println(lastUpdated);
			List<JSONObject> groups = new ArrayList<JSONObject>();
			groups.add(new JSONObject().put("S_NO", "1").put("CENTER_CODE", "114.01").put("CODE", "114.01.01").put("NAME", "Group 01").put("ENTRY_DATE", "2023-06-05"));
			groups.add(new JSONObject().put("S_NO", "2").put("CENTER_CODE", "114.01").put("CODE", "114.01.02").put("NAME", "Group 02").put("ENTRY_DATE", "2023-06-05"));
			groups.add(new JSONObject().put("S_NO", "3").put("CENTER_CODE", "114.02").put("CODE", "114.02.01").put("NAME", "Group 03").put("ENTRY_DATE", "2023-06-05"));
			groups.add(new JSONObject().put("S_NO", "4").put("CENTER_CODE", "114.02").put("CODE", "114.02.02").put("NAME", "Group 04").put("ENTRY_DATE", "2023-06-05"));
			groups.add(new JSONObject().put("S_NO", "5").put("CENTER_CODE", "114.03").put("CODE", "114.03.01").put("NAME", "Group 05").put("ENTRY_DATE", "2023-06-05"));
			groups.add(new JSONObject().put("S_NO", "6").put("CENTER_CODE", "114.03").put("CODE", "114.03.02").put("NAME", "Group 06").put("ENTRY_DATE", "2023-06-05"));
			groups.add(new JSONObject().put("S_NO", "7").put("CENTER_CODE", "115.01").put("CODE", "115.01.01").put("NAME", "Group 07").put("ENTRY_DATE", "2023-06-05"));
			groups.add(new JSONObject().put("S_NO", "8").put("CENTER_CODE", "115.01").put("CODE", "115.01.02").put("NAME", "Group 08").put("ENTRY_DATE", "2023-06-05"));
			groups.add(new JSONObject().put("S_NO", "9").put("CENTER_CODE", "115.02").put("CODE", "115.02.01").put("NAME", "Group 09").put("ENTRY_DATE", "2023-06-05"));
			groups.add(new JSONObject().put("S_NO", "10").put("CENTER_CODE", "115.02").put("CODE","115.02.02").put("NAME", "Group 10").put("ENTRY_DATE", "2023-06-05"));
			List<JSONObject> centers = new ArrayList<JSONObject>();
			centers.add(new JSONObject().put("S_NO", "1").put("CODE", "114.01").put("NAME", "Center 01").put("ENTRY_DATE", "2023-06-05").put("ORGANIZATION_CODE", "014"));
			centers.add(new JSONObject().put("S_NO", "2").put("CODE", "114.02").put("NAME", "Center 02").put("ENTRY_DATE", "2023-06-05").put("ORGANIZATION_CODE", "014"));
			centers.add(new JSONObject().put("S_NO", "3").put("CODE", "114.03").put("NAME", "Center 03").put("ENTRY_DATE", "2023-06-05").put("ORGANIZATION_CODE", "014"));
			centers.add(new JSONObject().put("S_NO", "4").put("CODE", "115.01").put("NAME", "Center 04").put("ENTRY_DATE", "2023-06-05").put("ORGANIZATION_CODE", "015"));
			centers.add(new JSONObject().put("S_NO", "5").put("CODE", "115.02").put("NAME", "Center 05").put("ENTRY_DATE", "2023-06-05").put("ORGANIZATION_CODE", "015"));
			List<JSONObject> members = new ArrayList<JSONObject>();
			members.add(new JSONObject().put("S_NO", "1").put("CODE", "114.01.01.01").put("FIRST_NAME", "Robert").put("MIDDLE_NAME", "Downey").put("LAST_NAME", "Jr").put("GROUP_CODE", "114.01.01").put("CLIENT_TYPE", "M").put("REGISTRATION_DATE", "2023-06-05").put("MOBILE_NO", "9848000001").put("PHONE_NO", "01-5437622").put("EMAIL", "robert.j@gmail.com").put("CURRENT_ADDRESS", "Aurahi Municipality-3, Na Tole, Mahotari, Mahotari").put("PERMANENT_ADDRESS", "Aurahi Municipality-3, Na Tole, Mahotari").put("DOB", "1956-06-05").put("CTZ_NO", "6543453434").put("CTZ_ISSUED_DATE", "1956-06-05").put("CTZ_ISSUED_FROM", "Taplegung").put("GENDER", "M").put("MARRITAL_STATUS", "M").put("FATHER_NAME", "Tom Jr").put("MOTHER_NAME", "Taylor Jr").put("GRAND_FATHER_NAME", "Thomas D Jr").put("SPOUSE_NAME", "Gwyneth Paltrow").put("SPOUSE_CTZ_NO", "6543453435").put("SPOUSE_CTZ_ISSUED_DATE", "1956-06-06").put("SPOUSE_CTZ_ISSUED_FROM", "Taplegung").put("FATHER_IN_LAW_NAME", "Kimly"));
			members.add(new JSONObject().put("S_NO", "2").put("CODE", "114.01.01.02").put("FIRST_NAME", "Gwyneth").put("MIDDLE_NAME", "").put("LAST_NAME", "Paltrow").put("GROUP_CODE", "114.01.01").put("CLIENT_TYPE", "M").put("REGISTRATION_DATE", "2023-06-05").put("MOBILE_NO", "9848000002").put("PHONE_NO", "01-5437623").put("EMAIL", "gwyneth.p@gmail.com").put("CURRENT_ADDRESS", "Aurahi Municipality-3, Na Tole, Mahotari, Mahotari").put("PERMANENT_ADDRESS", "Aurahi Municipality-3, Na Tole, Mahotari").put("DOB", "1956-06-06").put("CTZ_NO", "6543453435").put("CTZ_ISSUED_DATE", "1956-06-06").put("CTZ_ISSUED_FROM", "Doti").put("GENDER", "M").put("MARRITAL_STATUS", "M").put("FATHER_NAME", "Kiska Paltrow").put("MOTHER_NAME", "Sijoniria").put("GRAND_FATHER_NAME", "Fallcon").put("SPOUSE_NAME", "Robert Downey Jr").put("SPOUSE_CTZ_NO", "6543453435").put("SPOUSE_CTZ_ISSUED_DATE", "1956-06-06").put("SPOUSE_CTZ_ISSUED_FROM", "Kailali").put("FATHER_IN_LAW_NAME", "Dr Youjan"));
			members.add(new JSONObject().put("S_NO", "3").put("CODE", "114.01.02.01").put("FIRST_NAME", "Jon").put("MIDDLE_NAME", "").put("LAST_NAME", "Favreau").put("GROUP_CODE", "114.01.02").put("CLIENT_TYPE", "P").put("REGISTRATION_DATE", "2023-06-05").put("MOBILE_NO", "9848000003").put("PHONE_NO", "01-5437624").put("EMAIL", "jon.f@gmail.com").put("CURRENT_ADDRESS", "Gokulganga Rural Municipality-2, Goku, Ramechhap, Mahotari").put("PERMANENT_ADDRESS", "Gokulganga Rural Municipality-2, Goku, Ramechhap").put("DOB", "1956-06-07").put("CTZ_NO", "6543453436").put("CTZ_ISSUED_DATE", "1956-06-07").put("CTZ_ISSUED_FROM", "Taplegung").put("GENDER", "M").put("MARRITAL_STATUS", "D").put("FATHER_NAME", "The God Father").put("MOTHER_NAME", "Shakira").put("GRAND_FATHER_NAME", "Tommy Morgan").put("SPOUSE_NAME", "Terrence Howard").put("SPOUSE_CTZ_NO", "6543453435").put("SPOUSE_CTZ_ISSUED_DATE", "1956-06-06").put("SPOUSE_CTZ_ISSUED_FROM", "Taplegung").put("FATHER_IN_LAW_NAME", "The Strange"));
			members.add(new JSONObject().put("S_NO", "4").put("CODE", "114.01.02.02").put("FIRST_NAME", "Terrence").put("MIDDLE_NAME", "").put("LAST_NAME", "Howard").put("GROUP_CODE", "114.01.02").put("CLIENT_TYPE", "S").put("REGISTRATION_DATE", "2023-06-05").put("MOBILE_NO", "9848000004").put("PHONE_NO", "01-5437625").put("EMAIL", "terrence.h@gmail.com").put("CURRENT_ADDRESS", "Sundarbazar Municipality-9, bhatewodar, Lamjung, Mahotari").put("PERMANENT_ADDRESS", "Sundarbazar Municipality-9, bhatewodar, Lamjung").put("DOB", "1956-06-08").put("CTZ_NO", "6543453437").put("CTZ_ISSUED_DATE", "1956-06-08").put("CTZ_ISSUED_FROM", "Kanchanpur").put("GENDER", "F").put("MARRITAL_STATUS", "S").put("FATHER_NAME", "Terrifa").put("MOTHER_NAME", "Dumera").put("GRAND_FATHER_NAME", "Yoman Freeman").put("SPOUSE_NAME", "Jon Favreau").put("SPOUSE_CTZ_NO", "6543453435").put("SPOUSE_CTZ_ISSUED_DATE", "1956-06-06").put("SPOUSE_CTZ_ISSUED_FROM", "Morang").put("FATHER_IN_LAW_NAME", "Morgan Freeman"));
			members.add(new JSONObject().put("S_NO", "5").put("CODE", "115.01.01.01").put("FIRST_NAME", "Jeff").put("MIDDLE_NAME", "").put("LAST_NAME", "Bridges").put("GROUP_CODE", "115.01.01").put("CLIENT_TYPE", "M").put("REGISTRATION_DATE", "2023-06-05").put("MOBILE_NO", "9848000005").put("PHONE_NO", "01-5437626").put("EMAIL", "jeff.b@gmail.com").put("CURRENT_ADDRESS", "Bhanu Municipality-10, rampure, Tanahun, Mahotari").put("PERMANENT_ADDRESS", "Bhanu Municipality-10, rampure, Tanahun").put("DOB", "1956-06-09").put("CTZ_NO", "6543453438").put("CTZ_ISSUED_DATE", "1956-06-09").put("CTZ_ISSUED_FROM", "Taplegung").put("GENDER", "F").put("MARRITAL_STATUS", "S").put("FATHER_NAME", "Terrifa").put("MOTHER_NAME", "Jetliza").put("GRAND_FATHER_NAME", "Bethardy").put("SPOUSE_NAME", "Jefferi").put("SPOUSE_CTZ_NO", "6543453435").put("SPOUSE_CTZ_ISSUED_DATE", "1956-06-06").put("SPOUSE_CTZ_ISSUED_FROM", "Taplegung").put("FATHER_IN_LAW_NAME", "Jr Drapal"));
			members.add(new JSONObject().put("S_NO", "6").put("CODE", "115.01.02.02").put("FIRST_NAME", "Stan").put("MIDDLE_NAME", "").put("LAST_NAME", "Lee").put("GROUP_CODE", "115.01.02").put("CLIENT_TYPE", "M").put("REGISTRATION_DATE", "2023-06-05").put("MOBILE_NO", "9848000006").put("PHONE_NO", "01-5437627").put("EMAIL", "stan.lee@gmail.com").put("CURRENT_ADDRESS", "Rainas Municipality-6, Gaulitar, Lamjung, Mahotari").put("PERMANENT_ADDRESS", "Rainas Municipality-6, Gaulitar, Lamjung").put("DOB", "1956-06-10").put("CTZ_NO", "6543453439").put("CTZ_ISSUED_DATE", "1956-06-10").put("CTZ_ISSUED_FROM", "Taplegung").put("GENDER", "F").put("MARRITAL_STATUS", "W").put("FATHER_NAME", "Stan LE").put("MOTHER_NAME", "Jettisha").put("GRAND_FATHER_NAME", "F Stan LE").put("SPOUSE_NAME", "Satnlis").put("SPOUSE_CTZ_NO", "6543453435").put("SPOUSE_CTZ_ISSUED_DATE", "1956-06-06").put("SPOUSE_CTZ_ISSUED_FROM", "Taplegung").put("FATHER_IN_LAW_NAME", "Tak Lee"));
			data.put("GROUPS", groups);
			data.put("CENTERS", centers);
			data.put("MEMBERS", members);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return data;
	}
	
	public JSONObject prepareLoanData(String date) {
		JSONObject data = new JSONObject();
		try {
			List<JSONObject> loans = new ArrayList<>();
			loans.add(new JSONObject().put("S.No", "1").put("MEM_CODE", "114.01.01.01").put("MEM_NAME", "Robert").put("LN_ACC_NO", "114.01.01.01.LN1").put("LN_ODUE_DAYS", "720").put("LN_AMOUNT", "50000").put("LN_ACC_OPENING_DATE", "2021-01-15").put("LN_MATURITY_DATE", "2023-01-15").put("LN_PRODUCT_ID", "1").put("STAFF_CODE", "00113"));
			loans.add(new JSONObject().put("S.No", "2").put("MEM_CODE", "114.01.01.02").put("MEM_NAME", "Gwyneth").put("LN_ACC_NO", "114.01.01.02.LN1").put("LN_ODUE_DAYS", "635").put("LN_AMOUNT", "100000").put("LN_ACC_OPENING_DATE", "2021-04-21").put("LN_MATURITY_DATE", "2023-01-16").put("LN_PRODUCT_ID", "2").put("STAFF_CODE", "00960"));
			loans.add(new JSONObject().put("S.No", "3").put("MEM_CODE", "115.01.01.01").put("MEM_NAME", "Jeff").put("LN_ACC_NO", "115.01.01.01.LN1").put("LN_ODUE_DAYS", "517").put("LN_AMOUNT", "150000").put("LN_ACC_OPENING_DATE", "2021-08-18").put("LN_MATURITY_DATE", "2023-01-17").put("LN_PRODUCT_ID", "3").put("STAFF_CODE", "00705"));
			loans.add(new JSONObject().put("S.No", "4").put("MEM_CODE", "115.01.02.02").put("MEM_NAME", "Stan").put("LN_ACC_NO", "115.01.02.02.LN1").put("LN_ODUE_DAYS", "365").put("LN_AMOUNT", "200000").put("LN_ACC_OPENING_DATE", "2022-01-18").put("LN_MATURITY_DATE", "2023-01-18").put("LN_PRODUCT_ID", "4").put("STAFF_CODE", "00830"));
			data.put("LN_ACC", loans);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return data;
	}

	public String getCurrentTime() {
		return dateUtil.formatDateWithTime(new Date());
	}

	public  JSONObject prepareLoanDueDays(String date) {
		JSONObject data = new JSONObject();
		try {
			List<JSONObject> loanDueDays = new ArrayList<>();
	        loanDueDays.add(new JSONObject().put("S.No", "1").put("LN_ACC_NO", "114.01.01.01.LN1").put("LN_ODUE_DAYS", "721"));
	        loanDueDays.add(new JSONObject().put("S.No", "2").put("LN_ACC_NO", "114.01.01.02.LN1").put("LN_ODUE_DAYS", "636"));
	        loanDueDays.add(new JSONObject().put("S.No", "3").put("LN_ACC_NO", "115.01.01.01.LN1").put("LN_ODUE_DAYS", "518"));
	        loanDueDays.add(new JSONObject().put("S.No", "4").put("LN_ACC_NO", "115.01.02.02.LN1").put("LN_ODUE_DAYS", "366"));
	        data.put("LN_ACC_ODUE_DAYS",loanDueDays);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return data;
	}

}
