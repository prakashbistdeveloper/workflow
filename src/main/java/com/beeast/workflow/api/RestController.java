package com.beeast.workflow.api;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.beeast.workflow.util.DateUtil;
import com.beeast.workflow.util.TempDataUtil;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.Locale;

@org.springframework.web.bind.annotation.RestController
@RequestMapping("/extLoan")
public class RestController {

	@Autowired
	private DateUtil dateUtil;

	@Autowired
	private TempDataUtil dataUtil;

	@RequestMapping(method = RequestMethod.GET)
	public String home(Locale locale, Model model, HttpSession session) {
		return "Hello, ".concat(session.getId()).concat(" workflow application, ").concat("System time : ")
				.concat(dataUtil.getCurrentTime()).concat(" !");
	}

	@PostMapping("/memberData")
	public String getUpdatedData(@RequestBody String jsonData) {
		String date = dateUtil.formatDateWithTime(new Date());
		try {
			JSONObject jsonObject = new JSONObject(jsonData);
			if (jsonObject.has("LAST_UPDATED_AT")) {
				date = jsonObject.getString("LAST_UPDATED_AT");
			}
		} catch (JSONException e) {
		}
		return dataUtil.prepareTempData(date).toString();
	}
	
	@PostMapping("/loanAccounts")
	public String getUpdatedLoanData(@RequestBody String jsonData) {
		String date = dateUtil.formatDateWithTime(new Date());
		try {
			JSONObject jsonObject = new JSONObject(jsonData);
			if (jsonObject.has("LAST_UPDATED_AT")) {
				date = jsonObject.getString("LAST_UPDATED_AT");
			}
		} catch (JSONException e) {
		}
		return dataUtil.prepareLoanData(date).toString();
	}
	
	@PostMapping("/loanAccountDueDays")
	public String updateDueDays(@RequestBody String jsonData) {
		String date = dateUtil.formatDateWithTime(new Date());
		try {
			JSONObject jsonObject = new JSONObject(jsonData);
			if (jsonObject.has("LAST_UPDATED_AT")) {
				date = jsonObject.getString("LAST_UPDATED_AT");
			}
		} catch (JSONException e) {
		}
		return dataUtil.prepareLoanDueDays(date).toString();
	}

}
