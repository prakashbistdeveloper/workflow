package com.beeast.workflow;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import java.util.logging.Logger;
/**
 * 
 * @author prakash
 *
 */
@Configuration
public class AppConfig {

	@Bean
	public Logger logger() {
		return Logger.getLogger("SMS_LOG");
	}
	
}
