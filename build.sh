#!/bin/bash

# Parse command line arguments
for arg in "$@"
do
    case $arg in
        -p=*|--port=*)
        SERVER_PORT="${arg#*=}"
        shift # Remove --port= from processing
        ;;
        -u=*|--username=*)
        DB_USERNAME="${arg#*=}"
        shift # Remove --username= from processing
        ;;
        -pw=*|--password=*)
        DB_PASSWORD="${arg#*=}"
        shift # Remove --password= from processing
        ;;
        -d=*|--database=*)
        DB_NAME="${arg#*=}"
        shift # Remove --database= from processing
        ;;
	-l=*|--log=*)
        LOG="${arg#*=}"
        shift # Remove --log= from processing
        ;;
        *)
        # Unknown option
        ;;
    esac
done

# Set default values if not provided through command line arguments
SERVER_PORT=${SERVER_PORT:-8080}
DB_USERNAME=${DB_USERNAME:-root}
DB_PASSWORD=${DB_PASSWORD:-toor}
DB_NAME=${DB_NAME:-workflow}
LOG=${LOG:-/var/log/workflow/workflow.log}

# Set database configuration properties in application.properties file
sed -i "s/\(server\.port=\).*/\1${SERVER_PORT}/" src/main/resources/application.properties
sed -i "s/\(spring\.datasource\.url=jdbc:mysql:\/\/localhost:3306\/\).*/\1${DB_NAME}/" src/main/resources/application.properties
sed -i "s/\(spring\.datasource\.username=\).*/\1${DB_USERNAME}/" src/main/resources/application.properties
sed -i "s/\(spring\.datasource\.password=\).*/\1${DB_PASSWORD}/" src/main/resources/application.properties
sed -i "s:\(logging\.file\.name=\).*:\1${LOG}:" src/main/resources/application.properties

# Echo settings
echo "************************************************************"
echo "** Building SMS app with the following configuration:"
echo "** Server port: $SERVER_PORT"
echo "** Database name: $DB_NAME"
echo "** Database username: $DB_USERNAME"
echo "** Database password: ******"
echo "** Log file : $LOG"
echo "************************************************************"

# Build the JAR file
mvn clean package

# Run the JAR file
java -jar -Dspring.config.location=src/main/resources/application.properties target/workflow-V1.0.0.jar

