# Getting Started

### Deployment

Method1
1. Change values in application.properties $PROJECT_DIR/src/main/resources
2. build and package application into an executable jar
	$mvn clean package
3. Run application
	$java -jar target/workflow-V1.0.0.jar

Method2
1. Build using the script provided in project source "build.sh" using one of the following command
$./build.sh -p=8082 -u=root -pw=toor -d=workflow -l=/var/log/workflow/workflow.log
$./build.sh --port=8082 --username=root --password=toor --database=workflow --log=/var/log/workflow/workflow.log
Script will modily following values in appication.properties and build jar in the target directory
server.port with value of -p or --port
Database name in spring.datasource.url with -d or --database
spring.datasource.username with value of -u or --username
spring.datasource.password with value of -pw or --password
logging.file.name with value of -l or --l
This will make jar inside the target directory of source
2. Run application
$java -jar target/workflow-V1.0.0.jar

## Services
1. Ping Application (GET)
   http://localhost:8082/api

